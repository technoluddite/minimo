<?php

namespace controller;

use app\model\User;

class Frontend
{

    protected $view;

    function __construct()
    {
        $this->view = new \view\View();
    }




    //temp?
    public function categorie($categorie = "accueil")
    {

        if (isset($_GET['categorie']) && $_GET['categorie'] !== "accueil") {
            $categorie = parse_str($_GET['categorie']);
        }

        $page_repo = new \app\model\ArticleRepository();


        $articles = (($categorie === "accueil") ? $page_repo->all() : $page_repo->all($categorie));


        //temp ?
        $this->view->setVar('articles', $articles);
        $this->view->setVar('view', "categorie");
        echo $this->view->render();
    }

    public function article()
    {

        if (isset($_GET['id'])) {
            $page_repo = new \app\model\ArticleRepository();
            $result = $page_repo->read($_GET['id']);

            if ($result) {
                $article = new \app\model\Article($result);
                $this->view->setVar('article', $article);
                $this->view->setVar('view', 'article');


                $comment_repo = new \app\model\CommentRepository();

                //we insert any comment if necessary
                if(isset($_POST['comment_content']) && isset($_SESSION['user']) ){

                    $comment_repo->insertComment($_SESSION['user']->user_login(),$_POST['comment_content'],date('Y-m-d H:i:s'),$_GET['id']);

                }



                //we fetch the comments to this article

                $comment_rows = $comment_repo->getComments($result['id']);

                $comments = [];
                foreach($comment_rows as $comment){
                    array_push($comments, new \app\model\Comment($comment)); 
                }

                $this->view->setVar('comments',$comments);

            } else {
                $this->view->setVar('view', 'errors/404');
            }
        } else {
            $this->view->setVar('view', 'errors/404');
        }

        echo $this->view->render();
    }

    public function page_stat()
    {

        if (isset($_GET['id'])) {
            $page_repo = new \app\model\PageStatRepository();
            $result = $page_repo->read($_GET['id']);

            if ($result) {
                $page_stat = new \app\model\PageStat($result);
                $this->view->setVar('page-stat', $page_stat);
                $this->view->setVar('view', 'page-statique');
            } else {
                $this->view->setVar('view', 'errors/404');
            }
        } else {
            $this->view->setVar('view', 'errors/404');
        }

        echo $this->view->render();
    }

    public function register(){
        if(isset($_SESSION['user'])){
            $this->view->setVar('view','already-co');
            echo $this->view->render('connexion-template');
            return;
        }

        if(isset($_POST['user_login']) && isset($_POST['user_pass']) ){
            $user_repo = new \app\model\UserRepository();
            $id = $user_repo->register($_POST['user_login'],$_POST['user_pass']);
            if($id !== false && $id !== 'insert failed' && $id !== 'exist'){
                $_SESSION['user'] = new User(['id'=>$id,'user_login'=>$_POST['user_login']]); 
                $this->view->setVar('view','you-are-co');
                echo $this->view->render('connexion-template');
                return;
            }
            

        }

        $this->view->setVar('view','register');
        echo $this->view->render('connexion-template');
    }

    public function login(){
        if(isset($_SESSION['user'])){
            $this->view->setVar('view','already-co');
            echo $this->view->render('connexion-template');
            return;
        }

        if(isset($_POST['user_login'])&&isset($_POST['user_pass'])){
            $user_repo = new \app\model\UserRepository();
            $user_row = $user_repo->check_login($_POST['user_login'],$_POST['user_pass']);
            if($user_row){
                $_SESSION['user'] = new User($user_row);
                $this->view->setVar('view','you-are-co');
                echo $this->view->render('connexion-template');
                return;
            }

        }

        $this->view->setVar('view','login');
        echo $this->view->render('connexion-template');
    }


    public function logout(){
        if(isset($_SESSION['user'])){
            $this->view->setVar('exname',$_SESSION['user']->user_login());
        }
        session_destroy();
        session_start();
        $this->view->setVar('view','logout');
        
        echo $this->view->render('connexion-template');

        

    }



    public function error($errorMessage)
    {

        echo $errorMessage;
    }
}
