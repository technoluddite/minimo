<?php

namespace app\model;

require_once("Repository.php");

class CommentRepository extends Repository
{
  /**
   * Crée un commentaire dans la base de données
   */
  function create()
  {
  }

  /**
   * Récupère les commentaires liés à un article dans la base de données
   */
  function getComments($id)
  {
    $statement = $this->db->prepare('SELECT * from comments WHERE post_id=' . $id .' order by comment_date desc ');
    try {

      $statement->execute();
    } catch (\PDOException $e) {
      echo "Statement failed: " . $e->getMessage();
      return false;
    }

    $result = $statement->fetchAll(\PDO::FETCH_ASSOC); 

    return $result;
  }



  function insertComment($comment_name,$comment_content,$comment_date,$post_id){

    //insertion
    $statement = $this->db->prepare('insert into comments (post_id,comment_name,comment_content,comment_date) values (:post_id,:comment_name,:comment_content,:comment_date)');
        
    $statement->bindParam(':post_id',$post_id);
    $statement->bindParam(':comment_name',$comment_name);
    $statement->bindParam(':comment_content',$comment_content);
    $statement->bindParam(':comment_date',$comment_date);
    try {

      $statement->execute();

    } catch (\PDOException $e) {
      echo "Statement failed: " . $e->getMessage();
      return false;
    }


  }

  /**
   * Met un commentaire à jour dans la base de données
   */
  function update($name)
  {
  }

  /**
   * Efface un commentaire de la base de données
   */
  function delete($name)
  {
  }

}
