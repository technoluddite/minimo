<?php

namespace app\model;

 class Comment
{


    protected $id;
    protected $post_id;
    protected $comment_name;
    protected $comment_email;
    protected $comment_content;
    protected $comment_date;




    function __construct(array $datas = array()) {

        $this->hydrate($datas);
    }


    public function hydrate(array $datas)
    {


        foreach ($datas as $key => $value) {

            $method = 'set' . ucfirst($key);

            if (method_exists($this, $method)) {
            
                $this->$method($value);
            }
        }

    }

    public function id(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }


    public function post_id(){
        return $this->post_id;
    }

    public function setPost_id($post_id){
        $this->post_id = $post_id;
    }


    public function comment_name(){
        return $this->comment_name;
    }

    public function setComment_name($comment_name){
        $this->comment_name = $comment_name;
    }


    public function comment_email(){
        return $this->comment_email;
    }

    public function setComment_email($comment_email){
        $this->comment_email = $comment_email;
    }


    public function comment_content(){
        return $this->comment_content;
    }

    public function setComment_content($comment_content){
        $this->comment_content = $comment_content;
    }


    public function comment_date(){
        return $this->comment_date;
    }

    public function setComment_date($comment_date){
        $this->comment_date = $comment_date;
    }


}