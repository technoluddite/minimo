<?php

namespace app\model;

 class User
{


    protected $id;
    protected $user_login;

    function __construct(array $datas = array()) {

        $this->hydrate($datas);
    }


    public function hydrate(array $datas)
    {


        foreach ($datas as $key => $value) {

            $method = 'set' . ucfirst($key);

            if (method_exists($this, $method)) {
            
                $this->$method($value);
            }
        }

    }

    public function id(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function user_login(){
        return $this->user_login;
    }

    public function setUser_login($user_login){
        $this->user_login = $user_login;
    }




}