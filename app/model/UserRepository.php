<?php

namespace app\model;

class UserRepository extends Repository
{
  /**
   * Crée une page dans la base de données
   */
  function create()
  {
  }

  /**
   * vérifie l'existence d'un utilisateur dans la base de données
   */
  function check_login($user_login,$user_pass)
  {
    $statement = $this->db->prepare('SELECT id,user_login from users WHERE user_login=:user_login and user_pass=:user_pass');
    
    $statement->bindParam(':user_pass',$user_pass,\PDO::PARAM_STR);
    $statement->bindParam(':user_login',$user_login,\PDO::PARAM_STR);

    try {

      $statement->execute();

    } catch (\PDOException $e) {
      echo "Statement failed: " . $e->getMessage();
      return false;
    }

    return $statement->fetch(\PDO::FETCH_ASSOC);
  }

  function register($user_login,$user_pass){

    $this->db->beginTransaction();


    $statement = $this->db->prepare('SELECT count(*) from users WHERE user_login=:user_login');

    $statement->bindParam(':user_login',$user_login,\PDO::PARAM_STR);

    try {

      $statement->execute();

    } catch (\PDOException $e) {
      echo "Statement failed: " . $e->getMessage();
      $this->db->rollBack();
      return false;
    }

    //user_login already taken
    if($statement->fetch(\PDO::FETCH_NUM)[0]!=0){
      $this->db->rollBack();
      return 'exist';
    };


    //insertion
    $statement = $this->db->prepare('insert into users (user_login,user_pass) values (:user_login,:user_pass)');
    
    $statement->bindParam(':user_pass',$user_pass,\PDO::PARAM_STR);
    $statement->bindParam(':user_login',$user_login,\PDO::PARAM_STR);

    try {

      $statement->execute();

    } catch (\PDOException $e) {
      echo "Statement failed: " . $e->getMessage();
      $this->db->rollBack();
      return false;
    }



    $statement = $this->db->prepare('SELECT id from users WHERE user_login=:user_login and user_pass=:user_pass');

    $statement->bindParam(':user_login',$user_login,\PDO::PARAM_STR);
    $statement->bindParam(':user_pass',$user_pass,\PDO::PARAM_STR);

    try {

      $statement->execute();

    } catch (\PDOException $e) {
      echo "Statement failed: " . $e->getMessage();
      $this->db->rollBack();
      return false;
    }

    $id = $statement->fetch(\PDO::FETCH_NUM)[0];

    //insertion failed
    if($id===false){
      $this->db->rollBack();
      return 'insert failed';
    };

    $this->db->commit();



    return $id;

  }

  /**
   * Met une page à jour dans la base de données
   */
  function update($name)
  {
  }

  /**
   * Efface une page de la base de données
   */
  function delete($name)
  {
  }

}
