<?php

namespace app\model;

class Article extends Post
{

    protected $filepath;
    protected $alt;


    public function filepath(){
        return $this->filepath;
    }

    public function setFilepath($filepath){
        $this->filepath = $filepath;
    }

    public function alt(){
        return $this->alt;
    }

    public function setAlt($alt){
        $this->alt = $alt;
    }
    
}
