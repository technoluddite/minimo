<?php

namespace app\model;

abstract class Post
{
    protected $id;
    protected $author;
    protected $date;
    protected $content;
    protected $title;
    protected $status;
    protected $name;
    protected $category;

    function __construct(array $datas = array()) {

        $this->hydrate($datas);
    }


    public function hydrate(array $datas)
    {


        foreach ($datas as $key => $value) {

            $method = 'set' . ucfirst($key);

            if (method_exists($this, $method)) {
            
                $this->$method($value);
            }
        }

    }

    //getters

    public function title() {

        return $this->title;
    }

    public function content() {
        return $this->content;
    }

    public function category() {
        return $this->category;
    }

    public function author() {
        return $this->author;
    }


    //setters

    public function setId($id){
        $this->id = $id;
    }

    public function setAuthor($author){
        $this->author = $author;
    }

    public function setDate($date){
        $this->date = $date;
    }

    public function setContent($content){
        $this->content = $content;
    }

    public function setTitle($title){
        $this->title = $title;
    }

    public function setStatus($status){
        $this->status = $status;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function setCategory($category){
        $this->category = $category;
    }

}