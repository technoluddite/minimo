<?php

namespace app\model;

require_once("Repository.php");

class ArticleRepository extends Repository
{
  /**
   * Crée une page dans la base de données
   */
  function create()
  {
  }

  /**
   * Récupère une page dans la base de données
   */
  function read($id)
  {
    $statement = $this->db->prepare('SELECT id,author,date,content,title,status,name,type,category,posts_posts.post_id2 from posts join posts_posts on posts.id=posts_posts.post_id1 WHERE type="article" and id="' . $id . '"');

    try {

      $statement->execute();
    } catch (\PDOException $e) {
      echo "Statement failed: " . $e->getMessage();
      return false;
    }

    $result = $statement->fetch(\PDO::FETCH_ASSOC); 

    $statement = $this->db->prepare('SELECT name as filepath,title as alt from posts WHERE type="file" and id=' . $result['post_id2']);

    try {

      $statement->execute();
    } catch (\PDOException $e) {
      echo "Statement failed: " . $e->getMessage();
      return false;
    }

    $result = array_merge($result,$statement->fetch(\PDO::FETCH_ASSOC));


    return $result;
  }

  /**
   * Met une page à jour dans la base de données
   */
  function update($name)
  {
  }

  /**
   * Efface une page de la base de données
   */
  function delete($name)
  {
  }

  /**
   * Récupère une liste de pages depuis la base de données
   * @param $categories la catégorie à séléctionner
   */
  function all($categories = array(), $limit = 6, $offset = 0)
  {

    $query_str = 'SELECT id,author,date,content,title,status,name,type,category,posts_posts.post_id2 from posts join posts_posts on posts.id=posts_posts.post_id1 WHERE type="article"';


    //if $categories isn't empty, we have to return all categories within it.
    //we set up the query string.
    if (count($categories) > 0) {
      $query_str = $query_str . ' and ( ';


      $first_cat = array_shift($categories);
      $query_str = $query_str . 'category="' . $first_cat . '"';

      foreach ($categories as $category) {

        $query_str = $query_str . ' or category="' . $category . '"';

      }

      $query_str=$query_str.' )';

    }


    $query_str=$query_str.' order by date desc limit '.$offset.' , '.$limit;


    $statement = $this->db->prepare($query_str);

    try {

      $statement->execute();
    } catch (\PDOException $e) {
      echo "Statement failed: " . $e->getMessage();
      return false;
    }

    

    $articles_rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
    $count_articles = count($articles_rows);
    if($count_articles>0){
      $query_str = 'SELECT id, name as filepath,title as alt from posts WHERE type="file" and id IN ('.$articles_rows[0]['post_id2'];
      for($i = 1;$i<$count_articles;$i++){
        $query_str = $query_str.','.$articles_rows[$i]['post_id2'];
      }
      $query_str = $query_str . ')';
      $statement = $this->db->prepare($query_str);
      try {

        $statement->execute();
      } catch (\PDOException $e) {
        echo "Statement failed: " . $e->getMessage();
        return false;
      }
  
    }
    
    $files = [];
    foreach($statement->fetchAll(\PDO::FETCH_ASSOC) as $filerow){
      $id = $filerow['id'];
      unset($filerow['id']);
      $files[$id] = $filerow;

    }

    $articles = [];


    foreach($articles_rows as $data){
  
      $articles[] =  new \app\model\Article(array_merge($data,$files[$data['post_id2']]));

    }
    
    return $articles;

  }
}
