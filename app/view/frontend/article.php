<?php 
    $article = $this->data['article'];
    $comments = $this->data['comments'];
?>
<article>
    <h2><?= $article->title()?></h2>
    <p><?= $article->content()?></p>
    <img src="public/img/<?=$article->filepath()?>" alt="<?=$article->alt()?>" class='w-100 mt-4'>

    <div id='coms'>
        <?php
            foreach($comments as $comment){
                include "app/view/frontend/components/comment.php";
            }

            if(isset($_SESSION['user'])){
                include 'app/view/frontend/components/comment_form.php';
            }

        ?>
    </div>
    
</article>