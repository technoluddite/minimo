<?php


//display as much articles as wanted
function listArticles($component,$articles = array(),$number = "max",$offset = 0){

    if($number === "max"){
        $limit = count($articles) - 1;
    }else{
        $limit = min((count($articles) - 1),$number + $offset - 1);
    }

    for ($i = $offset; $i <= $limit; $i++) {
        $article = $articles[$i];
        include "app/view/frontend/components/".$component.".php";
    }


}