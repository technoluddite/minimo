<article>

    <img src="public/img/<?=$article->filepath()?>" alt="<?=$article->alt()?>">
    <h3><?=$article->category();?></h3>
    <h2><?= $article->title(); ?></h2>
    <p><?= $article->content(); ?></p>
</article>