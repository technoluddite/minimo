<nav class="navbar navbar-expand-sm navbar-light d-flex justify-content-between align-items-center p-0 title-principal" id='nav-top'>
  <a class="navbar-brand" href="#">
    <img src="public/img/logo_minimo.png" alt="logo MINIMO">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav d-flex justify-content-between flex-grow-1">
      <li class="nav-item active">
        <a class="nav-link" href="#">LIFESTYLE</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">PHOTODIARY</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">MUSIC</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">TRAVEL</a>
      </li>

      <?php 
      if(!isset($_SESSION['user'])){
      ?>
        <li class="nav-item active">
          <a class="nav-link" href="?type=login">Se connecter</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="?type=register">S'enregistrer</a>
        </li>
      <?php
      }else{
        ?>

        <li class="nav-item active">
          <a class="nav-link" href="?type=logout">Déconnexion</a>
        </li>
      

      <?php
      }
      ?>

    </ul>
  </div>
</nav>