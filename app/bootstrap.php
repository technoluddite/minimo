<?php 

//dislay PHP errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require "app/db_variables.php";


function autoload($class)
{
    //le nom de la class appellée avec le namespace est du type "Application\Models\Post". Avec la fonction substr on récupère la dernière partie du nom, après le \
    $class = substr($class, strrpos($class, '\\') + 1); 

    //on vérifie que le fichier existe et si oui on l'inclut
    if (is_file('app/model/'.$class . '.php')) {
        require_once 'app/model/'.$class . '.php';
    } 
    
    //on vérifie que le fichier existe et si oui on l'inclut
    if (is_file('app/view/'.$class . '.php')) {
        require_once 'app/view/'.$class . '.php';
    } 

    //on vérifie que le fichier existe et si oui on l'inclut
    if (is_file('app/controller/'.$class . '.php')) {
        require_once 'app/controller/'.$class . '.php';
    } 
}

spl_autoload_register('autoload'); // On enregistre la fonction en autoload pour qu'elle soit appelée dès qu'on essaie d'instancier une classe non déclarée.

