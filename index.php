<?php
require "app/bootstrap.php";



if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(!isset($_GET['type'])){
    $type = "categorie";
}else{
    $type = $_GET['type'];
}


$controller = new controller\Frontend();

if (is_callable(array($controller, $type))) { //on vérifie si la méthode "$action" existe dans le contrôleur "$controller"
    $controller->$type(); //si oui, on l'appelle
  } else {
      
    $controller->error("le type ".$type." est incorrect"); //si non, on appelle la méthode index(), ou une méthode d'erreur par exemple
 }
